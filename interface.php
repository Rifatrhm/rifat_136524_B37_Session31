<?php
interface Canfly{
    public function fly();
}
interface Canswim{
    public function swim();
}

class Bird{
    public function info(){
        echo "I am a {$this->name}\n";
        echo "I am an Bird\n";
    }
}
class Penguine extends Bird implements Canswim{
    public $name = "Penguine";
     public  function swim(){
         echo "i can swim";
     }
}

class Duck extends Bird implements Canfly,Canswim{
    public $name = "Duck";
    public function fly()
    {
    echo "i can fly ";
    }
    public function swim()
    {
    echo "i can swim";
    }
}

//$obj= new Dove();
//
//$obj->info();echo "<br>";
//$obj->fly();echo "<br>";
//$obj->swim();
function describe ($objBird)
{
    if ($objBird instanceof Bird) {
        $objBird->info();
    }
    if ($objBird instanceof Canfly) {
        $objBird->fly();
    }
    if ($objBird instanceof Canswim) {
        $objBird->swim();
    }
}
    describe(new Penguine());echo "<br>";
    describe(new Duck());
