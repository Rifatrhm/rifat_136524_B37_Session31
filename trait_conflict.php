<?php
trait A{
     public function smallTalk(){
         echo 'a';
     }
     public function bigtalk(){
         echo 'A';
     }
}
trait B{
     public function smalltalk(){
         echo 'b';
     }
     public function bigtalk(){
         echo 'B';
     }
}
trait C{
     public function smalltalk(){
         echo 'c';
     }
     public function bigtalk(){
         echo 'C';
     }
}

class talker{
    use A,B,C{
        B::smallTalk insteadof A,C;
        A::bigTalk insteadof B,C;
    }
}

class Alaise_talker{
    use A,B,C{
        B::smallTalk insteadof A,C;
        A::bigTalk insteadof B,C;
        B::smallTalk as talk;
        C::bigtalk as tro;

    }
}

$obj =new Alaise_talker();
$obj->bigtalk();
$obj->smalltalk();
$obj->talk();
$obj->tro();
?>